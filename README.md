# Ubuntu base container

## About

Base container for all JavaCraft containers, based on the Ubuntu 20.04. 

## Changelog

### 20.04-1
- Installed `curl`.
- Installed `wget`.

