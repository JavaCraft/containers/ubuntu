FROM ubuntu:20.04

RUN apt update && \
    apt install -y \
        curl \
        wget

RUN cd /tmp && \
    curl -L https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-`uname -s`-`uname -m` -o envsubst && \
    chmod +x envsubst && \
    mv envsubst /usr/local/bin

